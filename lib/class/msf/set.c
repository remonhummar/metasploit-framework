// Author : Ahmad Frendi (Jengkyy)
// 03-03-2019 03:46

#include <stdio.h>
#include <sys/socket.h>

typedef struct {
	EXPLOITS        exploits;
	PAYLOADS        payloads;
	RHOST           rhost;
	RPORT           rport;
	LHOST           lhost;
	LPORT           lport;
} msf;